# Awesome Pi-Hole List

A curated list of pi-hole block lists

## What is Pi-Hole
Pi-hole is a Linux network-level advertisement and internet tracker blocking application which acts as a DNS sinkhole, intended for use on a private network. It is designed for use on embedded devices with network capability, such as the Raspberry Pi, but can be used on other machines running Linux and cloud implementations.

Check it out (https://pi-hole.net/)